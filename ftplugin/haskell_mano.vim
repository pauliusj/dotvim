noremap <leader>s :call GHC_ShowInfo()<cr>
noremap <leader>mki :call GHC_MkImportsExplicit()<cr>
noremap <leader>ct :call GHC_CreateTagfile()<cr>
noremap <leader>T :call GHC_ShowType(1)<cr>
noremap <leader>t :call GHC_ShowType(0)<cr>
