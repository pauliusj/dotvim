syntax on " syntax highlighting
set nocompatible
filetype plugin indent on

set cf  " Enable error files & error jumping.
set clipboard+=unnamed  " Yanks go on clipboard instead.
set history=256  " Number of things to remember in history.
set autowrite  " Writes on make/shell commands
set ruler  " Ruler on
set nu  " Line numbers on
set nowrap  " Line wrapping off
set timeoutlen=250  " Time to wait after ESC (default causes an annoying delay)
colorscheme pablo "vividchalk   Uncomment this to set a default theme

" Formatting (some of these are for coding in C and C++)
set ts=4  " Tabs are 2 spaces
set shiftwidth=2  " Tabs under smart indent
set nocp incsearch
set cinoptions=:0,p0,t0
set cinwords=if,else,while,do,for,switch,case
set formatoptions=tcqr
set cindent
set autoindent
set smarttab
set expandtab

set t_Co=256

set guioptions-=T


" disable auto folding
set foldmethod=manual

fun SetupVAM()
  let c = get(g:, 'vim_addon_manager', {})
  let g:vim_addon_manager = c
  let c.plugin_root_dir = expand('$HOME') . '/.vim/vim-addons'
  let &rtp.=(empty(&rtp)?'':',').c.plugin_root_dir.'/vim-addon-manager'
  " let g:vim_addon_manager = { your config here see "commented version" example and help
  if !isdirectory(c.plugin_root_dir.'/vim-addon-manager/autoload')
    execute '!git clone --depth=1 git://github.com/MarcWeber/vim-addon-manager '
          \       shellescape(c.plugin_root_dir.'/vim-addon-manager', 1)
  endif
  call vam#ActivateAddons(['UltiSnips', 'unimpaired', 'Syntastic', 'snipmate-snippets', 'grep', 'Hoogle' , 'Align%294', 'AutomaticLaTeXPlugin', 'The_NERD_Commenter',  'The_NERD_tree', 'vim-addon-haskell', 'haskellmode-vim','Tagbar', 'taglist',  'Conque_Shell'], {'auto_install': 0})
  " removed haskellFold
endfun

call SetupVAM()

set showmatch  " Show matching brackets.
set mat=5  " Bracket blinking.
set novisualbell  " No blinking .
set noerrorbells  " No noise.
set laststatus=2  " Always show status line

"Extend line tab completion
set wildmenu
set wildmode=longest,full


set ic " ignore case in search
set incsearch " incremental search
set hlsearch " highlight search results
set smartcase " ignore case when lowercase

set tags=tags;/

autocmd FileType haskell setlocal tags+=/space/haskell/sources/packages-base/tags

let NERDTreeIgnore = ['\.project\.vim','\.synctex\.gz','\._log','\.aux','\.log','\.pdf','\._aux','\.bbl','\.blg']

" syntastic options
let g:syntastic_mode_map = {"mode": "active", "active_filetypes": [], "passive_filetypes": ["haskell"]}
map <C-B> :SyntasticCheck<CR>

:let Grep_Default_Filelist = '*.*'

" ATP options
"imap <silent> <buffer> <Tab> 		<C-R>=atplib#complete#TabCompletion(1)<CR>
let b:atp_TexOptions = ""

xmap <leader><Tab> :call UltiSnips_SaveLastVisualSelection()<CR>gvs
smap <leader><Tab> <Esc>:call UltiSnips_ExpandSnippetOrJump()<CR>  
imap <leader><Tab> <C-R>=UltiSnips_ExpandSnippetOrJump()<CR>    

let Tlist_Ctags_Cmd = "/usr/bin/ctags"
let Tlist_WinWidth = 50
map <silent> <Up> gk
imap <silent> <Up> <C-o>gk
map <silent> <Down> gj
imap <silent> <Down> <C-o>gj
map <silent> <home> g<home>
imap <silent> <home> <C-o>g<home>
map <silent> <End> g<End>
imap <silent> <End> <C-o>g<End>

nmap <F9> <C-W>c<CR>
map <F11> "+y
map <F12> "+gP
nnoremap <silent> <F3> :Grep<CR>
map <F4> :NERDTreeToggle<CR>
map <F5> :TlistToggle<cr>
map <F6> :TagbarToggle<CR>
"imap <leader><tab> <C-R>=snipMate#TriggerSnippet()<CR>

" don't insert comment leader automatically on new line
autocmd BufRead,BufNewFile * set formatoptions-=cro

" don't want comments at the beginning of the line in python
au BufNewFile,BufRead *.py set nocindent
au BufNewFile,BufRead *.py set nosmartindent
au BufNewFile,BufRead *.py set autoindent

" don't want strange indenting for LaTeX files
au BufNewFile,BufRead *.tex set nosmartindent
au BufNewFile,BufRead *.tex set wrap

" treat SConstruct as python
au BufNewFile,BufRead SConstruct set filetype=python


" change directory automatically
set autochdir

" filename auto completion
set wildmode=longest:full
set wildmenu

" show line numbers
set ruler

" Enable mouse
set mouse=a

" delete to the left in insert mode with backspace
set backspace=indent,eol,start

" always have some lines of text when scrolling
set scrolloff=5

" stop blinking cursor
set guicursor=a:blinkon0

" Use Caps_Lock as Escape
" --- add to ~/.Xmodmap: ---
" remove Lock = Caps_Lock
" keysym Caps_Lock = Escape

"quick window resize
"if bufwinnr(1)
" map + <C-W>+
" map - <C-W>-
"endif

"jump to first non-whitespace on line, jump to begining of line if already at first non-whitespace
map <Home> :call LineHome()<CR>:echo<CR>
imap <Home> <C-R>=LineHome()<CR>
map ^[[1~ :call LineHome()<CR>:echo<CR>
imap ^[[1~ <C-R>=LineHome()<CR>
function! LineHome()
  let x = col('.')
  execute "normal ^"
  if x == col('.')
    execute "normal 0"
  endif
  return ""
endfunction

" use ghc functionality for haskell files
au Bufenter *.hs compiler ghc

" configure browser for haskell_doc.vim
let g:haddock_browser = "chromium"

"change cursor in gnome terminal
if has("autocmd")
  au InsertEnter * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape ibeam"
  au InsertLeave * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape block"
  au VimLeave * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape ibeam"
endif
"change cursor in xterm

if &term =~ "xterm\\|rxvt"
  :silent !echo -ne "\033]12;red\007"
  let &t_SI = "\033]12;orange\007"
  let &t_EI = "\033]12;red\007"
  autocmd VimLeave * :!echo -ne "\033]12;red\007"
endif

nnoremap _ <Nop>

"enable ultisnip repo
"set runtimepath+=~/.vim/ultisnips_rep
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetDirectories=["UltiSnips", "/home/paulius/.vim/myUltiSnips"]
let g:UltiSnipsListSnippets="<c-l>"

vnoremap <C-r> "hy:%s/<C-r>h//gc<left><left><left>
